from flask import Flask
app = Flask(__name__)

@app.route("/")
def main():
    return "wellcom to index page!"

@app.route("/hello")
def hello():
    return "Hello flask!"

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)